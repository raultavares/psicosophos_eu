#!/usr/bin/env bash

# Backup originak files
# Add a clean parameter
# Alternate project name?

set -x

PROJECT_NAME=`basename $PWD`
#
sed -i '' 's/om-cms/'$PROJECT_NAME'/' .env
sed -i '' 's/om-cms/'$PROJECT_NAME'/' Dockerfile
sed -i '' 's/om-cms/'$PROJECT_NAME'/' docker-compose.yml
sed -i '' 's/OmCms/'$PROJECT_NAME'/' project/project/settings/development.py
sed -i '' 's/OmCms/'$PROJECT_NAME'/' project/project/settings/production.py
#sed -i '' 's/om-cms/'$PROJECT'/' ../scripts/code-linter.sh
#sed -i '' 's/om-cms/'$PROJECT'/' ../README.md
#sed -i '' 's/om-cms/'$PROJECT'/' ../scripts/restart-project.sh

echo "Bootstrapping project complete."
echo "Manually remove 'bootstrap_project.sh' when after bootstrapping project is complete."